import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hilbert

def generate_eeg_signal(duration, sampling_rate, frequency_hfo, hfo_amplitude, hfo_start, hfo_end):
    t = np.linspace(0, duration, int(duration * sampling_rate), endpoint=False)
    eeg_signal = 0.7 * np.sin(2 * np.pi * 4 * t)  # Phase normale (signal sinusoidal)
    hfo_mask = (t >= hfo_start) & (t < hfo_end)
    hfo_signal = hfo_amplitude * np.sin(2 * np.pi * frequency_hfo * t[hfo_mask])  # HFO anormale
    eeg_signal[hfo_mask] += hfo_signal
    eeg_signal += 0.2 * np.sin(2 * np.pi * 8 * t)  # Phase normale (signal sinusoidal)
    return t, eeg_signal, hfo_mask

def short_line_length(eeg_signal, window_size, threshold):
    line_length = np.zeros(len(eeg_signal))
    for i in range(window_size, len(eeg_signal),10):
        segment = eeg_signal[i - window_size:i]
        line_length[i] = np.sum(np.abs(np.diff(segment)))

    hfo_mask = line_length > threshold
    return hfo_mask

def mni_algorithm(eeg_signal, threshold):
    mni_index = np.abs(eeg_signal) / np.mean(np.abs(eeg_signal))
    hfo_mask = mni_index > threshold
    return hfo_mask

def hilbert_envelope(signal):
    analytic_signal = hilbert(signal)
    #analytic_signal = np.abs(analytic_signal)
    return analytic_signal

# Paramètres du signal EEG
duration = 10.0
sampling_rate = 1000
frequency_hfo = 250
hfo_amplitude = 0.3
hfo_start = 2.0  # Début de la phase avec HFO anormale en secondes
hfo_end = 5.0  # Fin de la phase avec HFO anormale en secondes

# Générer le signal EEG avec la HFO anormale
t, eeg_signal, hfo_mask = generate_eeg_signal(duration, sampling_rate, frequency_hfo, hfo_amplitude, hfo_start, hfo_end)

# Détection des HFO à l'aide de l'algorithme LTT
window_size = int(sampling_rate * 0.1)  # Taille de la fenêtre en échantillons (ici 0.1 seconde)
threshold = 10  # Seuil pour identifier les segments courts (à ajuster selon le signal)
hfo_detected_mask = short_line_length(eeg_signal, window_size, threshold)

threshold_mni = 2.0  # Seuil pour l'indice MNI (à ajuster selon le signal)
hfo_mask_mni = mni_algorithm(eeg_signal, threshold_mni)

envelope = hilbert_envelope(eeg_signal)


# Affichage du signal EEG avec les HFO détectées
plt.figure(figsize=(12, 4))
plt.plot(t, eeg_signal, label='Signal EEG')
#plt.plot(t[hfo_detected_mask], eeg_signal[hfo_detected_mask], 'ro', label='HFO détectée LTT')
plt.plot(t[hfo_mask_mni], eeg_signal[hfo_mask_mni], 'green', label='HFO détectée (MNI)')
#plt.plot(t, envelope, 'pink', label='Enveloppe d\'amplitude (Hilbert)')
plt.title("Détection des HFO avec l'algorithme LTT")
plt.xlabel("Temps (s)")
plt.ylabel("Amplitude")
plt.grid(True)
plt.legend()
plt.show()
